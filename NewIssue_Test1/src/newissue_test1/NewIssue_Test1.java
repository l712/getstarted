package newissue_test1;

/**
 *
 * @author Ling Ling Chang
 * @createdDate 2/26/2015
 * @lastModified 2/26/2015
 * 
 */
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class NewIssue_Test1 {

    /**
     * @param args the command line arguments
     */
    private static WebDriver driver=null;
    
    public static void main(String[] args) {
    
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        
        loginToJira("https://jira.atlassian.com","l712@yahoo.com","testing00");
        clickCreate();
        submitIssue();
    }
    
    public static void loginToJira(String url, String username, String password) {
        driver.get(url);
        driver.findElement(By.xpath("//li[@id='user-options']/a")).click();
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password); 
        driver.findElement(By.id("login-submit")).click();        
    }
    
    public static void clickCreate() {
        driver.findElement(By.id("create_link")).click();  
    }
    public static void submitIssue() {
        driver.findElement(By.id("summary")).sendKeys("Automated Testing");
        driver.findElement(By.id("create-issue-submit")).click();        
    }    
}
